%bcond_with check
%global debug_package %{nil}
%global commit0 2ad61761eff90597d7383312084e5805423680c7
%global shortcommit0 %(c=%{commit0}; echo ${c:0:7})

%global crate netavark

Name:           rust-%{crate}
Version:        0
Release:        0.1%{?dist}
Summary:        OCI network stack
License:        ASL 2.0
URL:            https://github.com/containers/%{crate}
Source:         %{url}/archive/%{commit0}/%{crate}-%{shortcommit0}.tar.gz

ExclusiveArch:  %{rust_arches}
%if %{__cargo_skip_build}
BuildArch:      noarch
%endif

BuildRequires:  rust-packaging

%global _description %{expand:
OCI network stack.}

%description %{_description}

%package -n %{crate}
Summary: %{summary}
License: ASL 2.0

%description -n %{crate} %{_description}

%files -n %{crate}
%license LICENSE
%dir %{_libexecdir}/podman
%{_libexecdir}/podman/%{crate}

%prep
%autosetup -n %{crate}-%{commit0} -p1
sed -i 's/nix = "0.23.0"/nix = "0.22.0"/' Cargo.toml
%cargo_prep

%generate_buildrequires
%cargo_generate_buildrequires

%build
%cargo_build

%install
%cargo_install

%changelog
* Tue Nov 23 2021 Lokesh Mandvekar <lsm5@fedoraproject.org> - 0-0.1
- Initial package
